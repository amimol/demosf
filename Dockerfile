FROM php:7.0-apache

ADD virtual-host.conf /etc/apache2/sites-enabled/000-default.conf

COPY . /app/
WORKDIR /app

RUN apt-get update && \
    apt-get -yq install git unzip && \
    rm -rf /var/lib/apt/lists/*

RUN a2enmod rewrite && \
    mkdir -p /app && rm -fr /var/www/html && \
    ln -s /app/web /var/www/html

RUN usermod -u 1000 www-data && \
    chown -R www-data:www-data /app/var/cache && \
    chown -R www-data:www-data /app/var/logs && \
    chown -R www-data:www-data /app/var/sessions

RUN php composer.phar install

EXPOSE 80
